import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

Rectangle{
    id:ccontrolbar
    default property alias content: layout.children
    property int margins:3
    property int spacing:3
    x:1
    color: "#f0f0f0"


    Row{
        id:layout
        width:parent.width
        height: parent.height
        anchors.margins: ccontrolbar.margins
        spacing:ccontrolbar.spacing
        }
}
