#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include<db_manager.h>
#include "treemodel.h"
#include"chart_class.h"
#include"tsa.h"
#include"MyTreeModel.hpp"
#include <QStringList>
#include <QDebug>
int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);
    qmlRegisterType<db_manager>("Db.manager.class", 1,0, "Db_manager");
 //   ImaginativeThinking::Tutorials::MyTreeModel t_obj;
//
//    QFile file(":/default.txt");
//       file.open(QIODevice::ReadOnly);
//       TreeModel model(file.readAll());
//       file.close();

 //   db_manager ob;
//  ob.execute__open_database("test_new");
 //   ob.execute__open_database("test_new");
    qmlRegisterType<ImaginativeThinking::Tutorials::MyTreeModel>("ca.imaginativethinking.tutorials.models", 1, 0, "MyTreeModel" );
   // t_obj.addEntry("tesmp","test");

//
//    int t_count= 3;//ob.table_count();
 //  ob.execute__open_database("test_new");
//    while(t_count>0){

//        t_obj.addEntry("test",ob.execute__list_tables(),"test");
//        t_count--;
//    }
//    chart_class ch;
//    QStringList temp;
//    temp<<ob.table_names();
//    for (QStringList::iterator it = temp.begin();
//            it != temp.end(); ++it) {
//           QString current = *it;
//           qDebug() << "[[" << current << "]]";
//       }
//    for(int i=0;i<3;i++){
//         qDebug() <<temp[i];
//    }


//    try{

//        ch.name("TEst");
//        ch.attach(*(ob.database_ptr()));
//        ch.enable_logs(tsa::os::path("F:\\rest"));  enable Logs
//        ch.run();
//        tsa::os::path output_dir = ch.log_path();
//        std::cout << "Strategy logs (with charts) were written to: " + output_dir.to_string() << std::endl;

//    }
//    catch (const std::exception& e)
//        {
//            std::cerr << e.what() << std::endl;
//        }

//    ob.execute__create_database("F:\\test_new");


  // ob.execute__create_table("tab2","open,close");
   // ob.execute__create_table("tab3","open,close");
  // ob.execute__list_tables();
  // ob.execute__display_dbname();
   // ob.execute__import_table_from_file("file:///F:/GC_NON.CSV","test","date(yyyymmdd),open,high, low, close, volume, tickcount");
 // ob.execute__table_print("test");
    QQmlApplicationEngine engine;
    //app.setWindowIcon(QIcon("window_icon.png"));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
