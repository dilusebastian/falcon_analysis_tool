import QtQuick 2.2
import QtQuick.Dialogs 1.1

MessageDialog {
    id: messageDialog
    property string m_title;
    property string m_text;
    property string m_icon;
    title: m_title
    text: m_text
    icon: m_icon
    onAccepted: {
        console.log("And of course you could only agree.")
        Qt.quit()
    }
    Component.onCompleted: visible = true
}
