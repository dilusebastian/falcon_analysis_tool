import QtQuick 2.10

import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

Rectangle {
    id:ctabpanel
    property string title: ""
    property bool grouped:false
    property color menu_color:"#f2f2f2"
    property color content_color:"white"
    property color seperator_color:"#d2d2d2"

    property alias menu:menu_holder.children
    default property alias content:content_holder.children
    property alias controlbar:control_bar.children

    property bool show_menu:true
    property bool show_controlbar:false
    property alias controlbar_height:control_bar.height
    property alias menu_width:menu_holder.width

    anchors.fill: parent;
    color: "white"


        Row{
            id:main_layout
            width: parent.width
            height: parent.height


            Rectangle{
                id:menu_holder
                height: ctabpanel.height
                width: 125
                color: menu_color
                visible: show_menu


            }
            Column{
                Rectangle{
                    id:seperator_bar_top
                    color: ctabpanel.menu_color
                    width: ctabpanel.width - menu_holder.width
                    height:0
                    visible: show_controlbar
                }
                Rectangle{
                    id:control_bar
                    width: ctabpanel.width - menu_holder.width
                    height: 43
                    color:ctabpanel.menu_color

                    visible: show_controlbar
                    border.width:0
                    border.color: "#eaeaea"
                    clip:true


                    BorderImage {
                        anchors { fill: parent;}
                        border { left: 7; right: 7 }
                        border { bottom: 10; top: 10 }

                        source: ad.image.menu_selected
                    }

                    /*Rectangle{
                        width: 3
                        height: control_bar.height
                        color: "white"

                        Rectangle{
                            width: 10
                            x:4
                            height: control_bar.height
                            color: "lightgray"
                        }
                    }*/

                }
                Rectangle{
                    id:seperator_bar_bottom
                    color: ctabpanel.seperator_color
                    width: ctabpanel.width - menu_holder.width
                    height:0
                    visible: show_controlbar
                }
                Rectangle{
                    id:content_holder
                    height:show_controlbar? ctabpanel.height-control_bar.height:ctabpanel.height
                    width: ctabpanel.width - menu_holder.width
                    color:content_color
                    border.width: 0
                    border.color: "#eaeaea"

                    /*Rectangle{
                        width: 1
                        height: ctabpanel.height
                        border.width: 0
                        border.color: "blue"
                        color: "white"
                    }*/

                   /* BorderImage {
                        anchors { fill: parent; leftMargin: 0; topMargin: 0; rightMargin: 0 }
                        border { left: 7; right: 7 }
                        source: ad.image.tab_normal
                    }*/
                }
            }



        }


}
