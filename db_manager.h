#ifndef DB_MANAGER_H
#define DB_MANAGER_H
#include <QObject>
#include <QStringList>
#include <QDebug>
#include"tsa.h"
class db_manager: public QObject
{

 private:
   std::unique_ptr<tsa::fast::database> m_db_ptr = nullptr;
 public:
       Q_OBJECT
    /* Database manager section */

 public:
 Q_INVOKABLE  std::string _db_path;
 Q_INVOKABLE  tsa::fast::database* database_ptr(void)const { return m_db_ptr.get(); }
 Q_INVOKABLE  db_manager();
 Q_INVOKABLE  void db_connect();
 Q_INVOKABLE  void db_list_tables();
 Q_INVOKABLE  void execute__create_database(const QString &_db_path);
 Q_INVOKABLE  QString execute__display_dbname();
 Q_INVOKABLE  void execute__open_database(const QString &_db_path);
 Q_INVOKABLE  void execute__close_database();
 Q_INVOKABLE  void execute__create_table(const std::string& _table_name, const std::string& _column_defs);
 Q_INVOKABLE  QString execute__list_tables() ;
 Q_INVOKABLE  void execute__import_table_from_file(QString file_path_, QString target_tbl, QString file_colmns);
 Q_INVOKABLE  QString execute__table_print(QString _table_name);
 Q_INVOKABLE  QStringList table_names();
 Q_INVOKABLE  bool db_open();
 Q_INVOKABLE  int table_count() ;
              std::string m_open_db_path;

// Q_INVOKABLE  void execute__import_bulk_table( const std::string& di_directory);
    //std::vector<std::string> table_names(void) const;



// /*chart Section */

//    tsa::in_stream mkt;
//    tsa::chart ch;
//    QString m_text;

// Q_INVOKABLE  void on_start();
//              void on_bar_close();


};

#endif // DB_MANAGER_H
