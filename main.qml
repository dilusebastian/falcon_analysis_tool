import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import Db.manager.class 1.0

import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.4
import ca.imaginativethinking.tutorials.models 1.0

ApplicationWindow {
    id:application_window
    visible: true
    width: 1565
    height: 835
    title: qsTr("DB Analysis Tool")
    Createdbpopup{
        id:cpop
    }
    MessageDialog{
        id:msg_dialog
    }

    Connectdbpopup{
        id:condb;
    }
    Printtable{
        id:p_table
    }

    File_dialog{
        id:f_dil
    }
    Cbutton{
        id:cbutton
    }

    Db_manager{
    id:db
    }

    MyTreeModel {
            id: theModel
        }
    Tree_view{
     id:t_v
    }




    menuBar: MenuBar {
        style: MenuBarStyle{
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                color:"#f0f0f0"
            }
        }
            Menu {
                title: "File"
                MenuItem { text: "Open..." }
                MenuItem { text: "Close" }
            }

            Menu {
                title: "Edit"
                MenuItem { text: "Cut" }
                MenuItem { text: "Copy" }
                MenuItem { text: "Paste" }
            }
        }


ColumnLayout{
 spacing: 0
   RowLayout{

        ToolBar {
             id:tbar
            style: ToolBarStyle {
                   padding {
                       left: 8
                       right: 8
                       top: 5
                       bottom: 3
                   }
                   background: Rectangle {
                       color:"#f0f0f0"
                   }
               }
            Layout.fillWidth: true;
                RowLayout {

                    ToolButton {
                        iconSource: "db_connection.png"
                        onClicked: {
                            cpop.show();

                        }
                    }
                    ToolButton {
                        iconSource: "db_connect.png"
                        onClicked: {
                            condb.show();

                        }
                    }
                    ToolButton {
                        iconSource: "data_table.png"
                        onClicked: {
                                f_dil.setVisible(true)
                                    }
                    }
                    ToolButton {
                        iconSource: "db_disconnect.png"
                        onClicked: {
//                            db.execute__close_database();

//                            lcol.tablestring=""
//                            lcol.textstring="Disconnected"
                            p_table.show();

                        }
                    }


                    ToolButton {
                        iconSource: "db_disconnect.png"
                        onClicked: {
//                            db.execute__close_database();

//                            lcol.tablestring=""
//                            lcol.textstring="Disconnected"
                          // db.execute__open_database(condb.db_path);
                            if(db.table_count()){
                               theModel.addEntry(condb.db_path,db.table_names());
                            }
                            else{
                                theModel.addEntry(condb.db_path,"Empty")
                            }


                        }
                    }
                }
            }

    }


    RowLayout{

        spacing: 0
        Lcolumn{
            id:lcol
            width:280
            height: application_window.height
            anchors.left: parent




        }
        Rcolumn{
            id:lcol2
            width:900
            height: application_window.height
            anchors.left: parent
        }


        Rcolumn{
            id:rcol
            width:application_window.width
            height: application_window.height
            anchors.right: parent
            Layout.fillWidth: parent

//            TreeView {
//                  id:treevu
//                  anchors.fill: parent
//                  model: theModel
//                  itemDelegate: Rectangle {
//                             color: ( styleData.row % 2 == 0 ) ? "white" : "lightblue"
//                             height: 20

//                             Text {
//                                 anchors.verticalCenter: parent.verticalCenter
//                                 text: styleData.value === undefined ? "" : styleData.value // The branches don't have a description_role so styleData.value will be undefined
//                             }
//                         }

//                  TableViewColumn {

//                      role: "name_role"
//                      title: "Name"

//                  }

////                  TableViewColumn {
////                      role: "description_role"
////                      title: "Description"
////                  }
//              }

        }








}
}




//    CcontrolBar{
//        x: 0
//        y: 0
//      width:application_window.width
//      height:86
//      color: "#f0f0f0"
//      Bborder{
//          width: parent.width
//          height: 1
//          anchors.top: parent.bottom
//      }

//      Rectangle {

//          id: rectangle

//          color: "blue"
//          clip: true
//          anchors.topMargin: 8
//          anchors.bottomMargin: 18
//          anchors.leftMargin: 8
//          anchors.rightMargin: 491
//          border.color: "grey"
//          anchors.fill: parent
//          MouseArea {
//              id: mouseArea
//              hoverEnabled: true
//              anchors.fill: parent


//               //onHoverEnabledChanged:  rectangle.color= "#c0dcf3"
//              onEntered: {
//                  rectangle.color= "red"
//              }
//              onExited: {

//                  rectangle.color = "green";
//             }
//              }


//          Text {
//              id: text1
//              x:6
//              y: 21
//              width: +rectangle.width
//              height: 21
//              text: qsTr("Define Data Sources")
//              font.pixelSize: 12
//              font.bold: true
//               color:"#8d9ed6"
//          }
//      }






    //}/* Control Bar close */
}/* application_window close */
