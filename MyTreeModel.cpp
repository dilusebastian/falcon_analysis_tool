/*
 * Copyright 2015 ImaginativeThinking
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MyTreeModel.hpp"

using namespace ImaginativeThinking::Tutorials;
db_manager ob;

MyTreeModel::MyTreeModel(QObject *parent) :
    QStandardItemModel(parent)
{
tree_run();
//addEntry("DB_name",ob.table_names());

}


/* ADD__ ENTRY FUNCTION */

void MyTreeModel::addEntry( const QString& branch,QStringList temp )
{  
    // childEntry->setData( description, MyTreeModel_Role_Description );
    // QString name;
    //std::string t_name[10];
    //temp<<ob.table_names();
    QStandardItem* entry = getBranch( branch );
    for (QStringList::iterator it = temp.begin();
         it != temp.end(); ++it) {
        QString current = *it;
        qDebug() << "[[" << current << "]]";
        auto childEntry = new QStandardItem( current);
        entry->appendRow( childEntry );
    }

}

QStandardItem *MyTreeModel::getBranch(const QString &branchName)
{
    QStandardItem* entry;

    auto entries = this->findItems( branchName );
    if ( entries.count() > 0 )
    {
        entry = entries.at(0);
    }
    else
    {
        entry = new QStandardItem( branchName );
        this->appendRow( entry );
    }
    return entry;
}

QHash<int, QByteArray> MyTreeModel::roleNames() const
{
    return m_roleNameMapping;
}
void MyTreeModel::tree_run(){

    m_roleNameMapping[MyTreeModel_Role_Name] = "name_role";
   m_roleNameMapping[MyTreeModel_Role_Description] = "description_role";

//addEntry("DB_name",ob.table_names());

}
