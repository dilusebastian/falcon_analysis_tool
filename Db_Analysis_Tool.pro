QT += quick
CONFIG += c++11
QT += widgets
RC_ICONS = window_icon.ico

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    db_manager.cpp \
    treeitem.cpp \
    treemodel.cpp \
    chart_class.cpp \
    MyTreeModel.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../work/libraries/TS_API__BETA_3.0.121/TS_API__BETA/lib/VS2017/release/ -lTS_API_LIB
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../work/libraries/TS_API__BETA_3.0.121/TS_API__BETA/lib/VS2017/debug/ -lTS_API_LIB
else:unix: LIBS += -L$$PWD/../work/libraries/TS_API__BETA_3.0.121/TS_API__BETA/lib/VS2017/ -lTS_API_LIB

INCLUDEPATH += $$PWD/../work/libraries/TS_API__BETA_3.0.121/TS_API__BETA/inc
DEPENDPATH += $$PWD/../work/libraries/TS_API__BETA_3.0.121/TS_API__BETA/inc

HEADERS += \
    db_manager.h \
    treeitem.h \
    treemodel.h \
    chart_class.h \
    MyTreeModel.hpp






#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../work/libraries/boost_1_60_0/stage/lib/ -llibboost_filesystem-vc140-mt-1_60
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../work/libraries/boost_1_60_0/stage/lib/ -llibboost_filesystem-vc140-mt-1_60d
#else:unix: LIBS += -L$$PWD/../work/libraries/boost_1_60_0/stage/lib/ -llibboost_filesystem-vc140-mt-1_60

#INCLUDEPATH += $$PWD/../work/libraries/boost_1_60_0
#DEPENDPATH += $$PWD/../work/libraries/boost_1_60_0
