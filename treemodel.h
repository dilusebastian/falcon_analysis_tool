#ifndef TREEMODEL_H
#define TREEMODEL_H
#include <QList>
#include<treeitem.h>
#include <QAbstractTableModel>
#include<db_manager.h>
class TreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit TreeModel(const QString &data, QObject *parent = 0);
    ~TreeModel();
    QVariant data(const QModelIndex &index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    std::string _db_path;
    db_manager *db_manager_ptr;
    QStringList table_names;

private:
    void setupModelData(QString &db_path, TreeItem *parent);

    TreeItem *rootItem;

};

#endif // TREEMODEL_H
