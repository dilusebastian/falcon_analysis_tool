#include "db_manager.h"

db_manager::db_manager()
{
}

void db_manager::db_connect(){

}


void db_manager::db_list_tables(){

}

void db_manager::execute__create_database(const QString& q_db_path)
{
     _db_path = q_db_path.toStdString();
    std::cout<<_db_path;
    Q_INVOKABLE _db_path;
    bool exists = tsa::fast::database::exists(_db_path);
    if (exists) {
        std::cout << std::endl << "database already exists: " << _db_path << std::endl;
        return;
    }
    else {
        try
        {
            tsa::fast::database db(_db_path);
            std::cout << std::endl << " database created" << std::endl;
        }
        catch (std::exception& e)
        {
            std::cout << e.what() << std::endl;

        }
    }

}



void db_manager::execute__open_database(const QString& q_db_path)
{
    _db_path = q_db_path.toStdString();
    bool path_exists = tsa::fast::database::exists(_db_path);
    if (!path_exists) {
        std::cout << std::endl << "database does not exists: " << _db_path << std::endl;
        return;
    }

    if (m_db_ptr) {
        m_db_ptr.release();

    }
    tsa_assert(!m_db_ptr);

    try
    {

        m_db_ptr = std::make_unique<tsa::fast::database>(_db_path);
        m_open_db_path = _db_path;
        std::cout << "database is open: " + _db_path << std::endl;
    }
    catch (std::exception& e)
    {
        std::cout << e.what() << std::endl;
    }
}

void db_manager::execute__close_database(){
    m_db_ptr=NULL;

}




void db_manager::execute__create_table(const std::string& _table_name, const std::string& _column_defs)
{
    if (!m_db_ptr) {
        std::cout << "no open database" << std::endl;
        return;
    }
    try {
        std::string col_defs = (_column_defs);
        m_db_ptr->create_table(_table_name, col_defs);
    }
    catch (const std::exception& e) {
        std::cout << e.what() << std::endl;
        return;
    }
    tsa_assert(m_db_ptr->table_exists(_table_name));
    std::cout << "table created" << std::endl;
}



QString db_manager::execute__list_tables() {


    std::ostringstream stream;
    if (!m_db_ptr) {
        std::cout << "no open database" << std::endl;


    }
    try {
        size_t data = m_db_ptr->table_count();
        int size = static_cast<int>(data);
        std::string t_nam[10];
        //INT64 size = m_db_ptr->table_count();
        if (size)
        {
            // int j=0;
            for(auto &i: m_db_ptr->table_names()){

               stream << i+"\n";
              // t_nam[j]=stream.str();
              // j++;

            }
          std::string t_names = stream.str() ;
          QString qs = QString::fromLocal8Bit(t_names.c_str());
           return qs;

        }
        else
            std::cout << "No Tables Found "<<std::endl;

    }
    catch (const std::exception& e) {
        std::cout << e.what() << std::endl;

    }
}

QString db_manager::execute__display_dbname(){
   std::string db_path= m_db_ptr->db_path().to_string();
   QString qs = QString::fromLocal8Bit(db_path.c_str());

 return qs;
}

int db_manager::table_count(){
    size_t data = m_db_ptr->table_count();
    int size = static_cast<int>(data);

    return size;
}


void db_manager::execute__import_table_from_file(QString file_path_, QString target_tbl, QString file_colmns)
{
     tsa::os::path f_path;

     f_path=file_path_.right(file_path_.length()-8).toStdString();
    if (!m_db_ptr) {
        std::cout << "no open database" << std::endl;
        return;
    }
    // create table <table name> import <path> as <format>

    tsa::file_import_params p;

    p.target_table = target_tbl.toStdString();
    p.file_path = f_path;
    p.file_columns = file_colmns.toStdString();
    /*std::cout << p.target_table<<std::endl;
    std::cout << p.file_columns << std::endl;
    std::cout << p.file_path<<std::endl;*/

    p.delete_existing_table = false; //global
    p.column_separator = ',';//global
    p.fix_duplicate_timestamps;//global
    p.null_string; //global
    p.max_records; //global
    p.skip_first_line;//global

    try {
        m_db_ptr->import(p);
        std::cout << "Import is completed successfully" << std::endl;
    }
    catch (const tsa::exception& e) {
        m_db_ptr->drop_table(p.target_table, tsa::silent_on_missing);
        std::cout << e.message() << std::endl;
        return;
    }
    tsa_assert(m_db_ptr->table_exists(p.target_table));
    //auto m = m_db_ptr->table_metrics(p.target_table);
}




QString db_manager::execute__table_print(QString _table_name) {
    std::string table_name;
    table_name=_table_name.toStdString();
     std::ostringstream stream;
    if (!m_db_ptr) {
        std::cout << "no open database" << std::endl;
        //return;
    }
    try {
        //m_db_ptr->print_table(table_name,std::cout);
       m_db_ptr->print_table(table_name,stream) ;
       {

           //stream << i+"\n";

        }
        std::string print_table = stream.str() ;
        QString qs = QString::fromLocal8Bit(print_table.c_str());
         return qs;
        //void convert_to_multi_tick_bars(unsigned num_ticks_per_bar, const std::string& source_table_name,
        //	const std::string& target_table_name, bool replace_existing_table = false);

    }
    catch (const std::exception& e) {
        std::cout << e.what() << std::endl;
        //return;
    }
}



//bulk import function


//void db_manager::execute__import_bulk_table( QStringList di_directory)
//{
//    if (!m_db_ptr) {
//        std::cout << "no open database" << std::endl;
//        return;
//    }
//    // create table <table name> import <path> as <format>

//   // boost::filesystem::path paths(di_directory);
//   // std::string ar[100];

//    //if (exists(paths))    // does path actually exist?
//    {

//       // std::cout << paths << " is a directory containing:\n";
////        for (int i = 0; i < 10; i++)
////        {
////            //std::copy(boost::filesystem::directory_iterator(paths), boost::filesystem::directory_iterator(), // directory_iterator::value_type
////                //std::ostream_iterator<boost::filesystem::directory_entry>(std::cout, "\n"));
////                std::copy(boost::filesystem::directory_iterator(paths), boost::filesystem::directory_iterator(), // directory_iterator::value_type
////                std::ostream_iterator<boost::filesystem::directory_entry>(std::cout<<(ar[i]), "\n"));

////        }
////        for (int i = 0; i < 10; i++)
////        {
////            std::cout << "help"+ar[i] ;
////        }

//    //}

//    tsa::file_import_params p;

//    //p.target_table = _regex.at(2).str();
//    //p.file_path = _regex.at(1).str();
//    //p.file_columns = _regex.at(0).str();
//    /*std::cout << p.target_table<<std::endl;
//    std::cout << p.file_columns << std::endl;
//    std::cout << p.file_path<<std::endl;*/

//    p.delete_existing_table = false; //global
//    p.column_separator = ',';//global
//    p.fix_duplicate_timestamps;//global
//    p.null_string; //global
//    p.max_records; //global
//    p.skip_first_line;//global

//    try {
//        m_db_ptr->import(p);
//        std::cout << "Import is completed successfully" << std::endl;
//    }
//    catch (const tsa::exception& e) {
//        m_db_ptr->drop_table(p.target_table, tsa::silent_on_missing);
//        std::cout << e.message() << std::endl;
//        return;
//    }
//    tsa_assert(m_db_ptr->table_exists(p.target_table));
//    //auto m = m_db_ptr->table_metrics(p.target_table);
//}

QStringList db_manager::table_names(){
QStringList tables;
size_t data = m_db_ptr->table_count();
int size = static_cast<int>(data);
if (size)
{
    for(auto &i: m_db_ptr->table_names()){

      std::string t_names =i;
      QString qs = QString::fromLocal8Bit(t_names.c_str());
      tables+=qs;


    }


    return tables;
 // std::string t_names = stream.str() ;
 // QString qs = QString::fromLocal8Bit(t_names.c_str());

}
}



bool db_manager::db_open(){
    if(m_db_ptr){
        return true;
    }
    else{
        return false;
    }

}



/* Chart section */

//void db_manager::on_start(){

//mkt.connect("test");
//}

//void db_manager::on_bar_close(){
//   ch<<tsa::chart::panel(500)<<tsa::plot::ohlc(mkt);
//}
